from django.http import request
from django.http import response
from django.shortcuts import render

mhs_name = 'Muhammad Fathan Muthahhari'

# Create your views here.


response = {'name': mhs_name, 'story': 4}


def index(request):
    return render(request, 'index_lab4.html', response)


def gallery(request):
    return render(request, 'lab4_new_page.html', response)
