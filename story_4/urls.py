from os import name, pardir
from django.urls import path
from . import views

app_name = 'story_4'

urlpatterns = [
    path('', views.index, name='index'),
    path('gallery', views.gallery, name='gallery')
]
