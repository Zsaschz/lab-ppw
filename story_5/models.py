from django.db import models

# Create your models here.


class MataKuliah(models.Model):
    nama = models.CharField(max_length=50, default="Kosong")
    dosen_pengajar = models.CharField(max_length=30, default="Kosong")
    jumlah_sks = models.IntegerField(default=0)
    deskripsi = models.CharField(max_length=100, default="Kosong")
    tahun_ajar = models.CharField(max_length=20, default="Kosong")
    ruang_kelas = models.CharField(max_length=20, default="Kosong")
