from django.db.models.query import NamedValuesListIterable
from django.http import response
from django.http import request
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from .models import MataKuliah
from .forms import Input_Form

# Create your views here.


def form(request):
    return render(request, 'form.html', {'form': Input_Form})


def list_matkul(request):
    form = Input_Form(request.POST or None)
    matkuls = MataKuliah.objects.all()
    if(form.is_valid and request.method == 'POST'):
        form.save()
    return render(request, 'list_matkul.html', {'matkuls': matkuls})


def update(request):
    return render(request, 'form.html', {'form': Input_Form})


def delete(request, id):
    p = MataKuliah.objects.get(id=id)
    MataKuliah.delete(p)
    return HttpResponseRedirect('/story_5')


def detail(request, id):
    p = MataKuliah.objects.get(id=id)
    return render(request, 'detail.html', {'matkul': p})
