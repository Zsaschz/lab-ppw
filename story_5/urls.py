from os import name, pardir
from django.urls import path
from . import views

app_name = 'story_5'

urlpatterns = [
    path('', views.list_matkul, name='list_matkul'),
    path('new/', views.form, name='form'),
    path('detail/<int:id>', views.detail, name='detail'),
    path('delete/<int:id>', views.delete, name='delete'),
]
