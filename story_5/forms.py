from django import forms
from .models import MataKuliah


class Input_Form(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = ['nama', 'dosen_pengajar', 'jumlah_sks',
                  'deskripsi', 'tahun_ajar', 'ruang_kelas']

    error_messages = {
        'required': 'Please Type'
    }

    nama = forms.CharField(label='nama', max_length=50, widget=forms.TextInput(
        attrs={'type': 'text'}))
    dosen_pengajar = forms.CharField(label='dosen', max_length=30, widget=forms.TextInput(
        attrs={'type': 'text'}))
    jumlah_sks = forms.IntegerField(widget=forms.TextInput(
        attrs={'type': 'number', 'min': 0, 'max': 24}))
    deskripsi = forms.CharField(label='deskripsi', max_length=100, widget=forms.TextInput(
        attrs={'type': 'text'}))
    tahun_ajar = forms.CharField(label='tahun', max_length=20, widget=forms.TextInput(
        attrs={'type': 'text', 'placeholder': 'Gasal 2019/2020'}))
    ruang_kelas = forms.CharField(label='kelas', max_length=20, widget=forms.TextInput(
        attrs={'type': 'text'}))
