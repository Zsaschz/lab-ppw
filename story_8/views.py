from django.shortcuts import render
import requests
from django.http.response import JsonResponse

# Create your views here.
def index(request):
    return render(request, 'story_8.html')

def get_book(request, keyword):
    url = f"https://www.googleapis.com/books/v1/volumes?q={keyword}"
    response = requests.get(url).json()
    return JsonResponse(response)
