from django.http import response
from django.test import TestCase, Client


# Create your tests here.
class Test(TestCase):
    def test_url_exist(self):
        response = Client().get('/story_8/')
        self.assertEquals(response.status_code, 200)

    def test_url_get(self):
        response = Client().get('/story_8/test')
        self.assertEquals(response.status_code, 200)

    def test_response_ajax(self):
        response = Client().get('/story_8/test').json()
        length = len(response['items'])
        self.assertEquals(length, 10)
