from os import name, pardir
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='story-3'),
    path('gallery', views.gallery)
]
