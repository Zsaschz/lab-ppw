from django.http import request
from django.http import response
from django.shortcuts import render

mhs_name = 'Muhammad Fathan Muthahhari'

# Create your views here.


def index(request):
    response = {'name': mhs_name}
    return render(request, 'index_lab3.html', response)


def gallery(request):
    return render(request, 'lab3_new_page.html')
