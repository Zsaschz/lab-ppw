from django.shortcuts import render

mhs_name = 'Muhammad Fathan Muthahhari'
npm = 1906293190

# Create your views here.


def index(request):
    response = {'name': mhs_name, 'npm': npm, 'story': 1}
    return render(request, 'index_lab1.html', response)
