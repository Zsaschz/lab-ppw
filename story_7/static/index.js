(function($) {
    
  let allPanels = $('.content').hide();
    
  $('h3').click(function() {
      $this = $(this).parent();
      $target =  $this.next();

      if(!$target.hasClass('active')){
         allPanels.removeClass('active').slideUp();
         $target.addClass('active').slideDown();
      }else{
        allPanels.removeClass('active').slideUp();
      }
      
    return false;
  });

})(jQuery);

for(let i = 0; i < $(".list_accordion").length; i++){
  $(".list_accordion")[i].addEventListener("click", function(e){
    this.classList.toggle('active');
  })
}

$(".down").click(function(e){
  e.preventDefault();
  let $this = $(this).parent().parent();
  $this.next().insertBefore($this);
})

$(".up").click(function(e){
  e.preventDefault();
  let $this = $(this).parent().parent();
  $this.prev().insertAfter($this);
})